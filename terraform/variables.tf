### ----- Variables definitions
variable environment {
  default = "prod"
}

variable aws_region {
  default = "eu-central-1"
}

variable default_tags {
  type = "map"

  default = {
    Owner      = "ANHO"
    Created_by = "Terraform"
  }
}


variable vpc_id {
  default = ""
}

variable vpc_cidr {
  default = ""
}

variable zone_id {
  default = ""
}

variable azs {
  default = ["eu-central-1a", "eu-central-1b"]
}

variable ec2_subnet_ids {
  default = []
}

variable ec2_private_ips {
  default = []
}

variable elk_subnet_ids {
  default = []
  }

variable ssh_keypair_arn {
  default = "arn:aws:xxx"
}

variable project_name {
  default = "elk"
}

variable elk_instance_type {
  default     = "m4.large.elasticsearch"
  description = "Elasticsearch instance type for data nodes in the cluster"
}

variable elk_instance_count {
  description = "Number of data nodes in the cluster"
  default     = "2"
}

variable es_port {
  default = "443"
}

variable ebs_volume_size {
  default = "10"
  description = "EBS volumes for data storage in GB"
}

variable ebs_volume_type {
  default     = "gp2"
  description = "Storage type of EBS volumes"
}

variable encrypt_at_rest_enabled {
  default     = true
  description = "Whether to enable encryption at rest"
}

variable encrypt_at_rest_kms_key_id {
  default     = "arn:aws:kms:xxxxxxxxxxx"
  description = "The KMS key ID to encrypt the Elasticsearch domain with"
}

variable dedicated_master_enabled {
  default     = false
  description = "Indicates whether dedicated master nodes are enabled for the cluster"
}

variable zone_awareness_enabled {
  default     = true
  description = "Enable zone awareness for Elasticsearch cluster"
}


###############
# logstash
###############

variable elk_secret_arn {
  default = "arn:aws:secretsmanager:eu-central-1:xxx"
}

variable ssh_keypair_arn {
  default = "arn:aws:secretsmanager:eu-central-1:xxx"
}

variable hostname {
  default = "logstash"
}

variable ec2_subnet_ids {
  default = []
}

variable ec2_private_ips {
  default = []
}

variable ec2_instance_type {
  default = ""
}

variable ec2_ami {
  default = ""
}

variable environment_volume_snapshots_active {
  default = true
}

variable "snapshots_execution_role_arn" {
  default = ""
}

variable ec2_root_volume_size {
  default = ""
}

variable ec2_ingres_rules {
  default = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow incomming SSH traffic"
    },
    {
      from_port   = 5040
      to_port     = 5050
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Logstash, filebeat traffic"
    },
  ]
}

variable ec2_egress_rules {
  default = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow outgoing connections to the Internet"
    },
  ]
}
