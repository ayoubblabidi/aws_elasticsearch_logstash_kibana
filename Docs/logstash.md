**Logstash** is the “L” in the ELK Stack — the world’s most popular log analysis platform and is responsible for aggregating 
data from different sources, processing it, and sending it down the pipeline, usually to be directly indexed in Elasticsearch.
(Import data, parse them, filter and prepare to JSON and export to ElasticSearch)



<img src="../images/logstasch1.png" alt="Your image title" float: right width="550"/>


Logstasch:
- parses, transforms and filters data as it passes through
- can derive structure from unstructured data
- can anonimize personal datq or excude it entirely
- can scale across many nodes
- can absorbs throughput from load spikes

<img src="../images/logstash2.png" alt="Your image title" float: right width="550"/>


Now typical usage in the context of elastic search will look something like this.
So the modern way of doing things, if you're going to be doing something like publishing web logs into
an elastic search cluster for visualization later on, would be to install a lightweight client called
"file beat" on the individual web server hosts, and then file beat would send data to your log stash cluster,
which would then buffer that up and parse things out, and do geo location and things like that, and then
send things into elastic search to be indexed


<img src="../images/logstash3.png" alt="Your image title" float: right width="550"/>



<img src="../images/kibana.png" alt="Your image title" float: right width="550"/>
