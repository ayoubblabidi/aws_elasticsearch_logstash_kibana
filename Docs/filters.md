
Example

```ruby


[root@els1 ~]# curl -H "Content-Type: application/json" -XGET "127.0.0.1:9200/movies/_search?pretty" -d '
{
"query": {
"bool": {
"must": {"match": {"genre": "Sci-Fi"}},
"filter": {"range": {"year": {"gte": 2010, "lt": 2015}}}
}
}
}'
{
  "took" : 32,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.640912,
    "hits" : [
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "109487",
        "_score" : 0.640912,
        "_source" : {
          "id" : "109487",
          "title" : "Interstellar",
          "year" : 2014,
          "genre" : [
            "Sci-Fi",
            "IMAX"
          ]
        }
      }
    ]
  }
}


```


Below is an example of query clauses being used in query and filter context in the search API. This query will match documents where all of the following conditions are met:

The title field contains the word search.
The content field contains the word elasticsearch.
The status field contains the exact word published.
The publish_date field contains a date from 1 Jan 2015 onwards.


```ruby
GET /_search
{
  "query": {    (1)
    "bool": {   (2)
      "must": [
        { "match": { "title":   "Search"        }},
        { "match": { "content": "Elasticsearch" }}
      ],
      "filter": [   (3)
        { "term":  { "status": "published" }},
        { "range": { "publish_date": { "gte": "2015-01-01" }}}
      ]
    }
  }
}
```

1.  The query parameter indicates query context.


2.  The bool and two match clauses are used in query context, which means that they are used to score how well each document matches.


3.  The filter parameter indicates filter context. Its term and range clauses are used in filter context. They will filter out documents which do not match, but they will not affect the score for matching documents.
