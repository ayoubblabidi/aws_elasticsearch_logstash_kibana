
Importing Data:

- stand-alone scripts can submit bulk documents via REST API
- **logstach** and **beats** can stream data from:
   - logs
   - S3
   - databases
   - and more 
- AWS systems can stream data vi a lambda or kinesis firehose
- any others Elasticsearch integration add-ons like: kafka, spark


1. Importing Data with a script:
  - read data from some distributes fs (files)
  - transform it into JSON bulk inserts
  - submit via HTTP/REST to your Elasticsearch server
  
  **Example - python script**
  
  
  ```python
  import csv
import re

csvfile = open('ml-latest-small/movies.csv', 'r')

reader = csv.DictReader( csvfile )
for movie in reader:
        print ("{ \"create\" : { \"_index\": \"movies\", \"_id\" : \"" , movie['movieId'], "\" } }", sep='')
        title = re.sub(" \(.*\)$", "", re.sub('"','', movie['title']))
        year = movie['title'][-5:-1]
        if (not year.isdigit()):
            year = "2016"
        genres = movie['genres'].split('|')
        print ("{ \"id\": \"", movie['movieId'], "\", \"title\": \"", title, "\", \"year\":", year, ", \"genre\":[", end='', sep='')
        for genre in genres[:-1]:
            print("\"", genre, "\",", end='', sep='')
        print("\"", genres[-1], "\"", end = '', sep='')
        print ("] }")
```



2. Importing data with client Librares. Free elasticsearch librares are available for pretty much any language:
   - **java** has client maintained by elastic.co
   - **python** has an elasticsearch package
   - elasticsearch **ruby**
   - elasticsearch.pm module for **pearl**
   

