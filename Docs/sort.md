
***SORT EXAMPLE***

```ruby

curl -H "Content-Type: application/json" -XGET "127.0.0.1:9200/movies/_search?sort=year&pretty"
{
  "took" : 19,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 5,
      "relation" : "eq"
    },
    "max_score" : null,
    "hits" : [
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "1924",
        "_score" : null,
        "_source" : {
          "id" : "1924",
          "title" : "Plan 9 from Outer Space",
          "year" : 1959,
          "genre" : [
            "Horror",
            "Sci-Fi"
          ]
        },
        "sort" : [
          1959
        ]
      },
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "58559",
        "_score" : null,
        "_source" : {
          "id" : "58559",
          "title" : "Dark Knight, The",
          "year" : 2008,
          "genre" : [
            "Action",
            "Crime",
            "Drama",
            "IMAX"
          ]
        },
        "sort" : [
          2008
        ]
      },
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "109487",
        "_score" : null,
        "_source" : {
          "id" : "109487",
          "title" : "Interstellar",
          "year" : 2014,
          "genre" : [
            "Sci-Fi",
            "IMAX"
          ]
        },
        "sort" : [
          2014
        ]
      },
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "122886",
        "_score" : null,
        "_source" : {
          "id" : "122886",
          "title" : "Star Wars: Episode VII - The Force Awakens",
          "year" : 2015,
          "genre" : [
            "Action",
            "Adventure",
            "Fantasy",
            "Sci-Fi",
            "IMAX"
          ]
        },
        "sort" : [
          2015
        ]
      },
      {
        "_index" : "movies",
        "_type" : "_doc",
        "_id" : "135569",
        "_score" : null,
        "_source" : {
          "id" : "135569",
          "title" : "Star Trek Beyond",
          "year" : 2016,
          "genre" : [
            "Action",
            "Adventure",
            "Sci-Fi"
          ]
        },
        "sort" : [
          2016
        ]
      }
    ]
  }
}```




Allows you to add one or more sorts on specific fields. Each sort can be reversed as well. The sort is defined on a per field level, with special field name for _score to sort by score, and _doc to sort by index order.

Assuming the following index mapping:

```ruby

PUT /my_index
{
    "mappings": {
        "properties": {
            "post_date": { "type": "date" },
            "user": {
                "type": "keyword"
            },
            "name": {
                "type": "keyword"
            },
            "age": { "type": "integer" }
        }
    }
}
```


```ruby

GET /my_index/_search
{
    "sort" : [
        { "post_date" : {"order" : "asc"}},
        "user",
        { "name" : "desc" },
        { "age" : "desc" },
        "_score"
    ],
    "query" : {
        "term" : { "user" : "kimchy" }
    }
}
```


Note
_doc has no real use-case besides being the most efficient sort order. So if you don’t care about the order in which documents are returned, then you should sort by _doc. This especially helps when scrolling.

Sort Valuesedit
The sort values for each document returned are also returned as part of the response.

Sort Orderedit
The order option can have the following values:

asc

Sort in ascending order

desc

Sort in descending order

The order defaults to desc when sorting on the _score, and defaults to asc when sorting on anything else.

Sort mode optionedit
Elasticsearch supports sorting by array or multi-valued fields. The mode option controls what array value is picked for sorting the document it belongs to. The mode option can have the following values:

min

Pick the lowest value.

max

Pick the highest value.

sum

Use the sum of all values as sort value. Only applicable for number based array fields.

avg

Use the average of all values as sort value. Only applicable for number based array fields.

median

Use the median of all values as sort value. Only applicable for number based array fields.

The default sort mode in the ascending sort order is min — the lowest value is picked. The default sort mode in the descending order is max — the highest value is picked.

Sort mode example usageedit
In the example below the field price has multiple prices per document. In this case the result hits will be sorted by price ascending based on the average price per document.

```ruby

PUT /my_index/_doc/1?refresh
{
   "product": "chocolate",
   "price": [20, 4]
}

POST /_search
{
   "query" : {
      "term" : { "product" : "chocolate" }
   },
   "sort" : [
      {"price" : {"order" : "asc", "mode" : "avg"}}
   ]
}
```

 
Sorting numeric fieldsedit
For numeric fields it is also possible to cast the values from one type to another using the numeric_type option. This option accepts the following values: ["double", "long", "date", "date_nanos"] and can be useful for cross-index search if the sort field is mapped differently on some indices.

Consider for instance these two indices:


```ruby

PUT /index_double
{
    "mappings": {
        "properties": {
            "field": { "type": "double" }
        }
    }
}
Copy as cURL
View in Console
 
PUT /index_long
{
    "mappings": {
        "properties": {
            "field": { "type": "long" }
        }
    }
}
Copy as cURL
View in Console
 
Since field is mapped as a double in the first index and as a long in the second index, it is not possible to use this field to sort requests that query both indices by default. However you can force the type to one or the other with the numeric_type option in order to force a specific type for all indices:

POST /index_long,index_double/_search
{
   "sort" : [
      {
        "field" : {
            "numeric_type" : "double"
        }
      }
   ]
}
```

 
In the example above, values for the index_long index are casted to a double in order to be compatible with the values produced by the index_double index. It is also possible to transform a floating point field into a long but note that in this case floating points are replaced by the largest value that is less than or equal (greater than or equal if the value is negative) to the argument and is equal to a mathematical integer.

This option can also be used to convert a date field that uses millisecond resolution to a date_nanos field with nanosecond resolution. Consider for instance these two indices:

```ruby

PUT /index_double
{
    "mappings": {
        "properties": {
            "field": { "type": "date" }
        }
    }
}
Copy as cURL
View in Console
 
PUT /index_long
{
    "mappings": {
        "properties": {
            "field": { "type": "date_nanos" }
        }
    }
}


```
